﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantsCore.Models
{
    public class Cart : Entity
    {
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
        public int UserId { get; set; }
        public int DishId { get; set; }
        public Dish Dish { get; set; }
        public bool Ordered { get; set; }
        public int? OrderId { get; set; }
        public Order Order { get; set; }
        public int Quantity { get; set; }

    }
}
