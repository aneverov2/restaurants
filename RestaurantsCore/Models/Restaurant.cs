﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RestaurantsCore.Models
{
    public class Restaurant : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public virtual ICollection<Dish> Dishes { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }

        public Restaurant()
        {
            Dishes = new List<Dish>();
            Comments = new List<Comment>();
            Ratings = new List<Rating>();
        }
        
    }
}
