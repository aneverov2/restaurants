﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantsCore.Models
{
    public class Rating : Entity
    {
        public int Value { get; set; }
        public int UserId { get; set; }
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}
