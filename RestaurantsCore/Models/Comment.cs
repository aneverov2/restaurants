﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantsCore.Models
{
    public class Comment : Entity
    {
        public int RestuarantId { get; set; }
        public Restaurant Restuarant { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
