﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantsCore.Models
{
    public class Order : Entity
    {
        public DateTime OrderDate { get; set; }
        public int TotalPrice { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

        public virtual ICollection<Cart> Carts { get; set; }

        public Order()
        {
            Carts = new List<Cart>();
        }
    }
}
