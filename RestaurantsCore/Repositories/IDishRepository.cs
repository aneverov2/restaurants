﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace RestaurantsCore.Repositories
{
    public interface IDishRepository : IRepository<Dish>
    {
    }
}
