﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestaurantsCore.Models;

namespace RestaurantsCore.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        List<Order> OrderIncludeCarts();
        IEnumerable<Order> GetOrderByUserId (int userId);
    }
}
