﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace RestaurantsCore.Repositories
{
    public interface IRestorauntRepository : IRepository<Restaurant>
    {
        Restaurant GetRestaurantIncludeDishes(int id);
    }
}
