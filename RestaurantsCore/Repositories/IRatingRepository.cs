﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace RestaurantsCore.Repositories
{
    public interface IRatingRepository : IRepository<Rating>
    {
        Rating GetByRestIdUserId(int restaurantId, int userId);
    }
}
