﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestaurantsCore.Models;

namespace RestaurantsCore.Repositories
{
    public interface ICartRepository : IRepository<Cart>
    {
        IEnumerable<Cart> GetCartsByUserId(int id);
        Task AddCart(Cart cart);
        Cart GetCartByUserIdRestaurantIdDishId(int userId, int restaurantId, int dishId);
        Cart GetCartByUserId(int userId);
    }
}
