﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace RestaurantsCore.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IRestorauntRepository Restaurants { get; }
        IDishRepository Dishes { get; }
        ICartRepository Carts { get; }
        IOrderRepository Orders { get; }
        ICommetRepository Comments { get; }
        IRatingRepository Ratings { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        //void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
