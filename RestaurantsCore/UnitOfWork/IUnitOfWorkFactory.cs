﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantsCore.UnitOfWork
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
