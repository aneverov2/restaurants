﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Restaurants.Domain.Dtos;
using Restaurants.Domain.Services;
using RestaurantsCore.Models;

namespace Restaurants.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: Order
        [Authorize(Roles = "administrator,user")]

        public ActionResult Index()
        {
            IEnumerable<OrderModel> orders = _orderService.GetAllOrders();
            return View(orders);
        }

        // GET: Order/Details/5
        [Authorize(Roles = "administrator,user")]

        public ActionResult Details()
        {
            OrderModel model = _orderService.GetOrder();
            return View(model);
        }

        // POST: Order/Create
        [Authorize(Roles = "user")]

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create()
        {
            var result = await _orderService.CreateOrder();
            if (!result.IsSuccess)
                return RedirectToAction("Index", "Cart", result.Errors);
            return RedirectToAction(nameof(Index));
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Order/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}