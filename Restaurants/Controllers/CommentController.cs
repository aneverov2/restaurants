﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Restaurants.Domain.Dtos;
using Restaurants.Domain.Services;

namespace Restaurants.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }
        [Authorize(Roles = "user")]
        public ActionResult Create(int id)
        {
            CommentModel model = new CommentModel {RestuarantId = id};
            return View(model);
        }

        // POST: Comment/Create
        [Authorize(Roles = "user")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CommentModel model)
        {
            var result = await _commentService.AddComment(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction("Details", "Restaurant", new { id = model.RestuarantId });
        }
    }
}