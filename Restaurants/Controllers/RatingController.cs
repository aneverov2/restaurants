﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Restaurants.Domain.Dtos;
using Restaurants.Domain.Services;

namespace Restaurants.Controllers
{

    public class RatingController : Controller
    {
        private readonly IRatingService _ratingService;

        public RatingController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> Create(int id, int rating)
        {
            RatingModel model = new RatingModel(){RestaurantId = id, Value = rating};
            var result = await _ratingService.AddRating(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction("Details", "Restaurant", new {id = model.RestaurantId});
        }
    }
}