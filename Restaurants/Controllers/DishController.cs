﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Restaurants.Domain.Dtos;
using Restaurants.Domain.Services;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Controllers
{
    [Authorize(Roles = "administrator")]
    public class DishController : Controller
    {
        private readonly IDishService _dishService;
        private readonly IRestaurantService _restaurantService;

        public DishController(IDishService dishService, IRestaurantService restaurantService)
        {
            _dishService = dishService;
            _restaurantService = restaurantService;
        }

        // GET: Dish
        public ActionResult Index()
        {
            var dishes = _dishService.GetDishes();
            return View(dishes);
        }

        // GET: Dish/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dish/Create
        public ActionResult Create()
        {
            var model = new DishModel
            {
                RestaurantsSelectList = new SelectList(_restaurantService.GetAllRestaurants(), "Id", "Name")
            };
            return View(model);

        }

        // POST: Dish/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DishModel model)
        {
            var result = await _dishService.AddOrUpdateDish(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction(nameof(Index));
        }

        // GET: Dish/Edit/5
        public  ActionResult Edit(int id)
        {
            var result = _dishService.GetById(id);
            result.RestaurantsSelectList = new SelectList(_restaurantService.GetAllRestaurants(), "Id", "Name");
            return View(result);
        }

        // POST: Dish/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DishModel model)
        {
            var result = await _dishService.AddOrUpdateDish(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction(nameof(Index));
        }

        // GET: Dish/Delete/5
        public ActionResult Delete(int id)
        {
            var result = _dishService.GetById(id);
            return View(result);
        }

        // POST: Dish/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(DishModel model)
        {
            var result = await _dishService.DeleteDish(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction(nameof(Index));
        }
    }
}