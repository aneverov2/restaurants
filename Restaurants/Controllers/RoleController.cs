﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Controllers
{
    [Authorize(Roles = "administrator")]
    public class RoleController : Controller
    {
        private AspNetRoleManager<Role> RoleManager { get; set; }
        private AspNetUserManager<User> UserManager { get; set; }


        public RoleController(AspNetRoleManager<Role> rolemanager, AspNetUserManager<User> userManager)
        {
            RoleManager = rolemanager;
            UserManager = userManager;
        }
        public ActionResult Index()
        {
            return View(RoleManager.Roles);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await RoleManager.CreateAsync(new Role
                {
                    Name = model.Name,
                });
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Не получилось создать роль");
                }
            }
            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            Role role = await RoleManager.FindByIdAsync(id.ToString());
            if (role != null)
            {
                return View(new RoleModel { Id = role.Id, Name = role.Name });
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<ActionResult> Edit(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                Role role = await RoleManager.FindByIdAsync(model.Id.ToString());
                if (role != null)
                {
                    role.Name = model.Name;
                    IdentityResult result = await RoleManager.UpdateAsync(role);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Не получилось изменить роль");
                    }
                }
            }
            return View(model);
        }

        public async Task<ActionResult> Delete(string id)
        {
            Role role = await RoleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await RoleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> SetRole(string userId)
        {
            User user = await UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                var userRoles = await UserManager.GetRolesAsync(user);
                var allRoles = RoleManager.Roles.ToList();
                SetRoleModel model = new SetRoleModel
                {
                    UserId = userId,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult> SetRole(string userId, List<string> roles)
        {
            User user = await UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                var userRoles = await UserManager.GetRolesAsync(user);
                var allRoles = RoleManager.Roles.ToList();
                var addedRoles = roles.Except(userRoles);
                var removedRoles = userRoles.Except(roles);

                await UserManager.AddToRolesAsync(user, addedRoles);

                await UserManager.RemoveFromRolesAsync(user, removedRoles);

                return RedirectToAction("UserList");
            }

            return NotFound();
        }

        public IActionResult UserList() => View(UserManager.Users.ToList());
    }
}