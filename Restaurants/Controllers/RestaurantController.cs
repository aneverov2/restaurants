﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Restaurants.Domain.Dtos;
using Restaurants.Domain.Services;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Controllers
{
    
    public class RestaurantController : Controller
    {
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }
        // GET: Restaurant
        public ActionResult Index()
        {
            List<RestaurantModel> restaurants = _restaurantService.GetAllRestaurants().ToList();
            return View(restaurants);
        }

        // GET: Restaurant/Details/5
        public ActionResult Details(int id)
        {
            var result = _restaurantService.RestaurantDetails(id);
            
            return View(result);
        }

        // GET: Restaurant/Create
        [Authorize(Roles = "administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Restaurant/Create
        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RestaurantModel model)
        {
            var result = await _restaurantService.CreateRestaurant(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction(nameof(Index));
        }

        // GET: Restaurant/Edit/5
        [Authorize(Roles = "administrator")]
        public ActionResult Edit(int id)
        {
            var res = _restaurantService.GetModel(id);
            return View(res);
        }

        // POST: Restaurant/Edit/5
        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(RestaurantModel model)
        {
            var result = await _restaurantService.EditRestaurant(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);

            return RedirectToAction(nameof(Index));
        }

        // GET: Restaurant/Delete/5
        [Authorize(Roles = "administrator")]
        public ActionResult Delete(int id)
        {
            RestaurantModel model = _restaurantService.GetModel(id);
            return View(model);
        }

        // POST: Restaurant/Delete/5
        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(RestaurantModel model)
        {
            var result = await _restaurantService.DeleteRestaurant(model);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction(nameof(Index));
        }
    }
}