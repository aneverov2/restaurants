﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Restaurants.Domain.Services;

namespace Restaurants.Controllers
{
    [Authorize(Roles = "user")]
    public class CartController : Controller
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        // GET: Cart
        public ActionResult Index()
        {
            var result = _cartService.CartsIncludeDish();
            return View(result);
        }

        // GET: Cart/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cart/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cart/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int dishId, int restaurantId)
        {
            var result = await _cartService.AddToCardAsync(dishId, restaurantId);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction("Index", new { id = restaurantId });
        }

        // GET: Cart/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Cart/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //GET: Cart/Delete/5
        //public async Task<IActionResult> Delete(int dishId, int restaurantId)
        //{
        //    var result = await _cartService.RemoveFromCardAsync(dishId, restaurantId);
        //    if (!result.IsSuccess)
        //        return BadRequest(result.Errors);
        //    return RedirectToAction(nameof(Index));
        //}

        // POST: Cart/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int dishId, int restaurantId)
        {
            var result = await _cartService.RemoveFromCardAsync(dishId, restaurantId);
            if (!result.IsSuccess)
                return BadRequest(result.Errors);
            return RedirectToAction(nameof(Index));
        }
    }
}