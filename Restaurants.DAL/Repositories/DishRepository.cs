﻿using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        public DishRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Dishes;
        }
    }
}
