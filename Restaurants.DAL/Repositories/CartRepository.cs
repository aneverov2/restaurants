﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class CartRepository : Repository<Cart>, ICartRepository
    {
        public CartRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Carts;
        }

        public IEnumerable<Cart> GetCartsByUserId(int id)
        {
            return _context.Carts.Include(c => c.Dish).Where(c => c.UserId == id && c.Ordered == false).ToList();
        }

        public Cart GetCartByUserId(int userId)
        {
            return _context.Carts.Include(c => c.Dish).FirstOrDefault(c => c.UserId == userId && c.Ordered == false);
        }

        public async Task AddCart(Cart cart)
        {
            Cart res = _context.Carts.FirstOrDefault(
                c => c.DishId == cart.DishId && c.RestaurantId == cart.RestaurantId && c.UserId == cart.UserId && c.Ordered == false);
            if (res == null)
            {
                await AddAsync(cart);
            }
            else
            {
                res.Quantity++;
            }
        }

        public Cart GetCartByUserIdRestaurantIdDishId(int userId, int restaurantId, int dishId)
        {
            return _context.Carts.FirstOrDefault(c =>
                c.DishId == dishId && c.RestaurantId == restaurantId && c.UserId == userId && c.OrderId == null);
        }
    }
}
