﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class RestaurantRepository: Repository<Restaurant>, IRestorauntRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Restaurants;
        }

        public Restaurant GetRestaurantIncludeDishes(int id)
        {
            return _context.Restaurants.Include(d => d.Dishes).FirstOrDefault(r => r.Id == id);
        }
    }
}
