﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Orders;
        }

        public List<Order> OrderIncludeCarts()
        {
            return _context.Orders.Include(o => o.Carts).ToList();
        }

        public IEnumerable<Order> GetOrderByUserId(int userId)
        {
            return _context.Orders.Where(o => o.UserId == userId);
        }
    }
}
