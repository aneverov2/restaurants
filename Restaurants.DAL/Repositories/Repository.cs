﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected ApplicationDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            DbSet = _context.Set<T>();
        }

        public async Task<T> AddAsync(T entity)
        {
            var entry = await DbSet.AddAsync(entity);

            return entry.Entity;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }
        //identity
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public virtual T GetById(int id)
        {
            return Queryable.FirstOrDefault(DbSet, e => e.Id == id);
        }

        public void Update(T entity)
        {
            _context.Update(entity);
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
        public void ExplicitLoading(Func<T, bool> func)
        {
            DbSet.Where(func).AsQueryable().Load();
        }
        public void LazyLoading()
        {
            DbSet.AsQueryable().Load();
        }
    }
}
