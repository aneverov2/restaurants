﻿using System;
using System.Collections.Generic;
using System.Text;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class CommentRepository: Repository<Comment>, ICommetRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Comments;
        }
    }
}
