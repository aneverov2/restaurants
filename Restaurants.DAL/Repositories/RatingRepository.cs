﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;

namespace Restaurants.DAL.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Ratings;
        }


        public Rating GetByRestIdUserId(int restaurantId, int userId)
        {
            return DbSet.FirstOrDefault(e => e.RestaurantId == restaurantId && e.UserId == userId);
        }
    }
}
