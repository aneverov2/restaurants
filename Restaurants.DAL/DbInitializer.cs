﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Restaurants.DAL.DbContext;
using RestaurantsCore.Models;

namespace Restaurants.DAL
{
    public static class DbInitializer
    {
        public static async void Seed(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();

                UserManager<User> userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();
                RoleManager<Role> roleManager = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();

                User user = new User() { UserName = "admin"};
                user.PasswordHash = userManager.PasswordHasher.HashPassword(user, "qwerty");

                Role administrator = new Role(){Name = "administrator"};
                Role roleUser = new Role(){Name = "user" };

                if (!context.Roles.Any())
                {
                    await roleManager.CreateAsync(administrator);
                    await roleManager.CreateAsync(roleUser);
                }

                if (!context.Users.Any())
                {
                    await userManager.CreateAsync(user);
                    await userManager.AddToRoleAsync(user, administrator.Name);
                }
            }
        }
    }
}
