﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using Restaurants.DAL.DbContext;
using Restaurants.DAL.Repositories;
using RestaurantsCore.Models;
using RestaurantsCore.Repositories;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositories;

        private IDbContextTransaction _transaction;

        private bool _disposed;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();

            Restaurants = new RestaurantRepository(context);
            Dishes = new DishRepository(context);
            Carts = new CartRepository(context);
            Orders = new OrderRepository(context);
            Comments = new CommentRepository(context);
            Ratings = new RatingRepository(context);
        }

        public IRestorauntRepository Restaurants { get; }
        public IDishRepository Dishes { get; }
        public ICartRepository Carts { get; }
        public IOrderRepository Orders { get; }
        public ICommetRepository Comments { get; }
        public IRatingRepository Ratings { get; }

        public Task<int> CompleteAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void BeginTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        //public void BeginTransaction(IsolationLevel level)
        //{
        //    _transaction = _context.Database.BeginTransaction(level);
        //}

        public void CommitTransaction()
        {
            if (_transaction == null) return;

            _transaction.Commit();
            _transaction.Dispose();

            _transaction = null;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return _repositories.GetOrAdd(typeof(TEntity), (object)new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        public void RollbackTransaction()
        {
            if (_transaction == null) return;

            _transaction.Rollback();
            _transaction.Dispose();

            _transaction = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}
