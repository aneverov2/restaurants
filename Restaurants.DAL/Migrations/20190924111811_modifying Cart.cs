﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Restaurants.DAL.Migrations
{
    public partial class modifyingCart : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DishId",
                table: "Carts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Carts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RestaurantId",
                table: "Carts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Carts_DishId",
                table: "Carts",
                column: "DishId");

            migrationBuilder.AddForeignKey(
                name: "FK_Carts_Dishes_DishId",
                table: "Carts",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carts_Dishes_DishId",
                table: "Carts");

            migrationBuilder.DropIndex(
                name: "IX_Carts_DishId",
                table: "Carts");

            migrationBuilder.DropColumn(
                name: "DishId",
                table: "Carts");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Carts");

            migrationBuilder.DropColumn(
                name: "RestaurantId",
                table: "Carts");
        }
    }
}
