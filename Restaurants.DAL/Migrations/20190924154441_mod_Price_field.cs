﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Restaurants.DAL.Migrations
{
    public partial class mod_Price_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Price",
                table: "Dishes",
                nullable: false,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "Dishes",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
