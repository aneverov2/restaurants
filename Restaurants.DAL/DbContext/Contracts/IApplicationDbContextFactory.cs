﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurants.DAL.DbContext.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
