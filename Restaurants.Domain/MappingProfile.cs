﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using AutoMapper;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Domain
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            RestaurantMapping();
            DishMapping();
            RoleMapping();
            OrderMapping();
            CartMapping();
            CommentMapping();
            RatigMapping();
        }

        private void RestaurantMapping()
        {
            CreateMap<RestaurantModel, Restaurant>();
            CreateMap<Restaurant, RestaurantModel>();
                //.ForMember(x => x.Path, opt => opt.Ignore());
        }
        private void DishMapping()
        {
            CreateMap<DishModel, Dish>();
            CreateMap<Dish, DishModel>();
        }
        private void RoleMapping()
        {
            CreateMap<RoleModel, Role>();
            CreateMap<Role, RoleModel>();
        }
        private void CartMapping()
        {
            CreateMap<Cart, CartDto>()
                .ForMember(c => c.Dish, opt => opt.MapFrom(c => c.Dish.Name))
                .ForMember(c => c.Restaurant, opt => opt.MapFrom(c => c.Restaurant.Name))
                .ForMember(c => c.DishPrice, opt => opt.MapFrom(c => c.Dish.Price));

        }
        private void OrderMapping()
        {
            CreateMap<CartModel, Order>();
                //.ForMember(c => c.OrderDate, opt => opt.NullSubstitute(DateTime.Now));
            CreateMap<Order, CartModel>();
            CreateMap<OrderModel, Order>();
                //.ForMember(om => om.TotalPrice, opt => opt.MapFrom(o => o.Carts.Aggregate(0, (current, item) => (current + item.DishPrice * item.Quantity))));
            CreateMap<Order, OrderModel>();
        }
        private void CommentMapping()
        {
            CreateMap<Comment, CommentModel>();//.ForMember(cm => cm.UserName, opt => opt.MapFrom(c => c.User.UserName));

            CreateMap<CommentModel, Comment>();
            //.ForMember(c => c.Restuarant, opt => opt.Ignore())
        }
        private void RatigMapping()
        {
            CreateMap<RatingModel, Rating>();
            CreateMap<Rating, RatingModel>();
        }
    }
}
