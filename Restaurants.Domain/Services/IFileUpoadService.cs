﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Restaurants.Domain.Services
{
    public interface IFileUpoadService
    {
        void Upload(string path, string fileName, IFormFile file);
        void Remove(string path);
    }
}
