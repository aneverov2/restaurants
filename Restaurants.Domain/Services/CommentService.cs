﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Domain.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CommentService(IUnitOfWorkFactory unitOfWorkFactory, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<CommentModel> GetComments(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<EntityOperationResult<Comment>> AddComment(CommentModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
                    Comment comment = AutoMapper.Mapper.Map<Comment>(model);
                    comment.UserId = userId;
                    await uow.Comments.AddAsync(comment);
                    await uow.CompleteAsync();
                    return EntityOperationResult<Comment>.Success(comment);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Comment>.Failure().AddError(e.Message);
                }
            }
        }
    }
}
