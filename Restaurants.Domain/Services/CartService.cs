﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace Restaurants.Domain.Services
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartService(IUnitOfWorkFactory unitOfWorkFactory, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _httpContextAccessor = httpContextAccessor;
        }

        public CartModel CartsIncludeDish()
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                string userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
                List<Cart> list = uow.Carts.GetCartsByUserId(Convert.ToInt32(userId)).ToList();
                uow.Restaurants.LazyLoading();
                CartModel model = new CartModel()
                {
                    Carts = AutoMapper.Mapper.Map<List<CartDto>>(list),
                    TotalPrice = list.Aggregate(0, (current, item) => (current + item.Dish.Price * item.Quantity))

                };
                return model;
            }
            
        }

        public IEnumerable<Cart> GetCarts()
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                string userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
                IEnumerable<Cart> carts = uow.Carts.GetCartsByUserId(Convert.ToInt32(userId));
                return carts;
            }
        }

        public async Task<EntityOperationResult<Cart>> AddToCardAsync(int dishId, int restaurantId)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    if (dishId != 0)
                    {
                        string userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
                        Cart cart = uow.Carts.GetCartByUserId(Convert.ToInt32(userId));
                        if (cart == null || cart.RestaurantId == restaurantId)
                        {
                            cart = new Cart()
                            {
                                UserId = Convert.ToInt32(userId),
                                DishId = dishId,
                                RestaurantId = restaurantId,
                                Ordered = false,
                                Quantity = 1
                            };
                            await uow.Carts.AddCart(cart);
                            await uow.CompleteAsync();
                            return EntityOperationResult<Cart>.Success(cart);
                        }

                        return EntityOperationResult<Cart>.Failure().AddError("Вы не можете заказать одновременно блюда из разных ресторанов.");
                    }

                    return EntityOperationResult<Cart>.Failure().AddError("Необходимо выбрать блюдо");

                }
                catch (Exception e)
                {
                    return EntityOperationResult<Cart>.Failure().AddError(e.Message);
                }
                
            }
        }

        public async Task<EntityOperationResult<Cart>> RemoveFromCardAsync(int dishId, int restaurantId)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
                    Cart cart = uow.Carts.GetCartByUserIdRestaurantIdDishId(userId, restaurantId, dishId);
                    if (cart.Quantity > 1)
                    {
                        cart.Quantity--;
                    }
                    else
                    {
                        uow.Carts.Delete(cart);
                    }
                    await uow.CompleteAsync();
                    return EntityOperationResult<Cart>.Success(cart);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Cart>.Failure().AddError(e.Message);
                }

            }
        }
    }
}
