﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Restaurants.Domain.Services
{
    public class FileUploadService : IFileUpoadService
    {
        private readonly IHostingEnvironment _environment;

        public FileUploadService(IHostingEnvironment environment)
        {
            this._environment = environment;
        }
        public async void Upload(string path, string fileName, IFormFile file)
        {
            string newPath = Path.Combine(_environment.WebRootPath, path);
            Directory.CreateDirectory(newPath);
            using (var stream = new FileStream(Path.Combine(newPath, fileName), FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
        }

        public void Remove(string path)
        {
            string fullPath = Path.Combine(_environment.WebRootPath, path);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
        }
    }
}
