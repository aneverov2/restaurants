﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Domain.Services
{
    public class DishService : IDishService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public DishService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public DishModel GetById(int id)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Dish dish = uow.Dishes.GetById(id);
                return Mapper.Map<DishModel>(dish);
            }
        }

        public async Task<EntityOperationResult<Dish>> AddOrUpdateDish(DishModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    Dish dish = Mapper.Map<DishModel, Dish>(model);
                    if (model.Id == 0)
                    {
                        await uow.Dishes.AddAsync(dish);
                    }
                    else
                    {
                        uow.Dishes.Update(dish);
                    }
                    await uow.CompleteAsync();
                    return EntityOperationResult<Dish>.Success(dish);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Dish>.Failure().AddError(e.Message);
                }
                
            }
        }

        //public async Task<EntityOperationResult<Dish>> UpdateDish(DishModel model)
        //{
        //    using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
        //    {
        //        try
        //        {
        //            Dish dish = Mapper.Map<DishModel, Dish>(model);
        //            uow.Dishes.Update(dish);
        //            await uow.CompleteAsync();
        //            return EntityOperationResult<Dish>.Success(dish);
        //        }
        //        catch (Exception e)
        //        {
        //            return EntityOperationResult<Dish>.Failure().AddError(e.Message);
        //        }

        //    }
        //}

        public IEnumerable<DishModel> GetDishes()
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    IEnumerable<Dish> dishes = uow.Dishes.GetAll();
                    return Mapper.Map<IEnumerable<DishModel>>(dishes);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        public async Task<EntityOperationResult<Dish>> DeleteDish(DishModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    Dish dish = uow.Dishes.GetById(model.Id);
                    uow.Dishes.Delete(dish);
                    await uow.CompleteAsync();
                    return EntityOperationResult<Dish>.Success(dish);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Dish>.Failure().AddError(e.Message);
                }

            }
        }
    }
}
