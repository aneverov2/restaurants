﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Services
{
    public interface IDishService
    {
        DishModel GetById(int id);
        Task<EntityOperationResult<Dish>> AddOrUpdateDish(DishModel model);
        //Task<EntityOperationResult<Dish>> UpdateDish(DishModel model);
        IEnumerable<DishModel> GetDishes();
        Task<EntityOperationResult<Dish>> DeleteDish(DishModel model);
    }
}
