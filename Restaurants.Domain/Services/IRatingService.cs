﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Services
{
    public interface IRatingService
    {
        Task<EntityOperationResult<Rating>> AddRating(RatingModel model);
    } 
}
