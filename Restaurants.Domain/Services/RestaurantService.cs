﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Domain.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileUpoadService _fileUpoadService;
        private readonly IRatingService _ratingService;

        public RestaurantService(IUnitOfWorkFactory unitOfWorkFactory, IFileUpoadService fileUpoadService, IRatingService ratingService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileUpoadService = fileUpoadService;
            _ratingService = ratingService;
        }

        public IEnumerable<RestaurantModel> GetAllRestaurants()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var restaurants = unitOfWork.Restaurants.GetAll();
                return Mapper.Map<IEnumerable<RestaurantModel>>(restaurants);
            }
        }

        public RestaurantModel GetModel(int id)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Restaurant restaurant = uow.Restaurants.GetRestaurantIncludeDishes(id);
                RestaurantModel model = AutoMapper.Mapper.Map<Restaurant, RestaurantModel>(restaurant);
                return model;
            }
        }

        public async Task<EntityOperationResult<Restaurant>> CreateRestaurant(RestaurantModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    string path = $"images/{model.Name}";
                    Restaurant restaurant = Mapper.Map<RestaurantModel, Restaurant>(model);

                    restaurant.ImagePath = $"{path}/{model.Path.FileName}";
                    _fileUpoadService.Upload(path, model.Path.FileName, model.Path);

                    await uow.Restaurants.AddAsync(restaurant);
                    await uow.CompleteAsync();
                    return EntityOperationResult<Restaurant>.Success(restaurant);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Restaurant>.Failure().AddError(e.Message);
                }
            }
        }

        public async Task<EntityOperationResult<Restaurant>> EditRestaurant(RestaurantModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    string path = $"images\\{model.Name}";
                    string oldPath = GetPathById(model.Id);
                    Restaurant restaurant = Mapper.Map<RestaurantModel, Restaurant>(model);
                    
                    if (model.Path == null)
                    {
                        restaurant.ImagePath = oldPath;
                    }
                    else
                    {
                        restaurant.ImagePath = $"{path}/{model.Path.FileName}";
                        _fileUpoadService.Remove(oldPath);
                        _fileUpoadService.Upload(path, model.Path.FileName, model.Path);
                    }
                    uow.Restaurants.Update(restaurant);
                    await uow.CompleteAsync();

                    return EntityOperationResult<Restaurant>.Success(restaurant);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Restaurant>.Failure().AddError(e.Message);
                }
            }
        }

        public RestaurantModel RestaurantDetails(int id)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Restaurant restaurant = uow.Restaurants.GetRestaurantIncludeDishes(id);
                //uow.Comments.ExplicitLoading(r => r.RestuarantId == restaurant.Id);
                uow.Comments.LazyLoading();
                uow.Ratings.ExplicitLoading(r => r.RestaurantId == id);
                double raiting = restaurant.Ratings.Where(x => x.RestaurantId == id).Sum(x => x.Value);
                var length = restaurant.Ratings.Count(x => x.RestaurantId == id);
                if (length == 0)
                {
                    raiting = 0;
                }
                else
                {
                    raiting = raiting / length;
                }

                RestaurantModel model = Mapper.Map<Restaurant, RestaurantModel>(restaurant);
                model.Rating = raiting;

                return model;
            }
        }

        public async Task<EntityOperationResult<Restaurant>> DeleteRestaurant(RestaurantModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    Restaurant restaurant = uow.Restaurants.GetById(model.Id);
                    if (restaurant != null)
                    {
                        uow.Restaurants.Delete(restaurant);
                        await uow.CompleteAsync();
                        _fileUpoadService.Remove(restaurant.ImagePath);
                    }
                    return EntityOperationResult<Restaurant>.Success(restaurant);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Restaurant>.Failure().AddError(e.Message);
                }
            }
        }

        public string GetPathById(int id)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    Restaurant restaurant = uow.Restaurants.GetById(id);
                    return restaurant.ImagePath;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }
    }
}
