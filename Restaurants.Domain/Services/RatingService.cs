﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Domain.Services
{
    public class RatingService : IRatingService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RatingService(IHttpContextAccessor httpContextAccessor, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _httpContextAccessor = httpContextAccessor;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Rating>> AddRating(RatingModel model)
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    model.UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
                    Rating rating = uow.Ratings.GetByRestIdUserId(model.RestaurantId, model.UserId);
                    if (rating != null)
                    {
                        return EntityOperationResult<Rating>.Failure().AddError("Вы больше не можете изменять рейтинг");
                        
                    }

                    rating = Mapper.Map<Rating>(model);
                    await uow.Ratings.AddAsync(rating);
                    await uow.CompleteAsync();
                    return EntityOperationResult<Rating>.Success(rating);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Rating>.Failure().AddError(e.Message);
                }
            }
        }
    }
}
