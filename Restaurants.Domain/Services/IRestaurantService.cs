﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Services
{
    public interface IRestaurantService
    {
        IEnumerable<RestaurantModel> GetAllRestaurants();
        RestaurantModel GetModel(int id);
        Task<EntityOperationResult<Restaurant>> CreateRestaurant(RestaurantModel model);
        Task<EntityOperationResult<Restaurant>> EditRestaurant(RestaurantModel model);
        RestaurantModel RestaurantDetails(int id);
        Task<EntityOperationResult<Restaurant>> DeleteRestaurant(RestaurantModel model);
    }
}
