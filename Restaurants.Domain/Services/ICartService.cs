﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Services
{
    public interface ICartService
    {
        CartModel CartsIncludeDish();
        IEnumerable<Cart> GetCarts();
        Task<EntityOperationResult<Cart>> AddToCardAsync(int dishId, int restaurantId);
        Task<EntityOperationResult<Cart>> RemoveFromCardAsync(int dishId, int restaurantId);
    }
}
