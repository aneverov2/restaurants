﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;
using RestaurantsCore.UnitOfWork;

namespace Restaurants.Domain.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICartService _cartService;
        private readonly AspNetUserManager<User> _userManager;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory, IHttpContextAccessor httpContextAccessor, ICartService cartService, AspNetUserManager<User> userManager)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _httpContextAccessor = httpContextAccessor;
            _cartService = cartService;
            _userManager = userManager;
        }

        public IEnumerable<OrderModel> GetAllOrders()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (_httpContextAccessor.HttpContext.User.IsInRole("user"))
                {
                    var orders = unitOfWork.Orders.GetOrderByUserId(userId);
                    unitOfWork.Carts.ExplicitLoading(c => c.UserId == userId);
                    unitOfWork.Restaurants.LazyLoading();
                    unitOfWork.Dishes.LazyLoading();
                    return Mapper.Map<IEnumerable<OrderModel>>(orders);
                }
                else
                {
                    var orders = unitOfWork.Orders.GetAll();
                    unitOfWork.Carts.LazyLoading();
                    unitOfWork.Restaurants.LazyLoading();
                    unitOfWork.Dishes.LazyLoading();
                    return Mapper.Map<IEnumerable<OrderModel>>(orders);
                }
            }
        }

        public async Task<EntityOperationResult<Order>> CreateOrder()
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
                    IEnumerable<Cart> carts = _cartService.GetCarts();
                    if (carts.Any())
                    {
                        Order order = new Order()
                        {
                            TotalPrice = carts.Aggregate(0, (current, item) => (current + item.Dish.Price * item.Quantity)),
                            OrderDate = DateTime.Now,
                            UserId = userId
                        };
                        foreach (var cart in carts)
                        {
                            cart.Ordered = true;
                            cart.Order = order;
                            uow.Carts.Update(cart);
                        }

                        await uow.Orders.AddAsync(order);
                        await uow.CompleteAsync();
                        return EntityOperationResult<Order>.Success(order);
                    }

                    return EntityOperationResult<Order>.Failure().AddError("Необходимо добавить блюда в корзину");
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Order>.Failure().AddError(e.Message);
                }
            }
        }

        public OrderModel GetOrder()
        {
            throw new NotImplementedException();
        }
    }
}
