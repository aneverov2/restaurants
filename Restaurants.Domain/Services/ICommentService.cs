﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Restaurants.Domain.Dtos;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Services
{
    public interface ICommentService
    {
        IEnumerable<CommentModel> GetComments(int id);
        Task<EntityOperationResult<Comment>> AddComment(CommentModel model);
    }
}
