﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Dtos
{
    public class SetRoleModel
    {
        public string UserId { get; set; }
        public List<Role> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public SetRoleModel()
        {
            AllRoles = new List<Role>();
            UserRoles = new List<string>();
        }
    }
}
