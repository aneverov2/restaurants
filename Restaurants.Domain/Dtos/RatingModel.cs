﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurants.Domain.Dtos
{
    public class RatingModel : Model
    {
        public int Value { get; set; }
        public int UserId { get; set; }
        public int RestaurantId { get; set; }
    }
}
