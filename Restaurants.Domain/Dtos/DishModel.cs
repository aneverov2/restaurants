﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Restaurants.Domain.Dtos
{
    public class DishModel : Model
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int RestaurantId { get; set; }
        public SelectList RestaurantsSelectList { get; set; }
    }
}
