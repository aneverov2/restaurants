﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Dtos
{
    public class OrderModel : Model
    {
        public DateTime OrderDate { get; set; }
        public int TotalPrice { get; set; }
        public int UserId { get; set; }
        //public User User { get; set; }

        public virtual ICollection<CartDto> Carts { get; set; }

        public OrderModel()
        {
            Carts = new List<CartDto>();
        }
    }
}
