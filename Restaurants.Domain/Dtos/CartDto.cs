﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Dtos
{
    public class CartDto
    {
        public int RestaurantId { get; set; }
        public string Restaurant { get; set; }
        public int UserId { get; set; }
        public int DishPrice { get; set; }
        public int DishId { get; set; }
        public string Dish { get; set; }
        public bool Ordered { get; set; }
        public int? OrderId { get; set; }
        public int Quantity { get; set; }
    }
}
