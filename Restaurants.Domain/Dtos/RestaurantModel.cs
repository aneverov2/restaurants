﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Dtos
{
    public class RestaurantModel : Model
    {
        public string Name { get; set; }
        public IFormFile Path { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public double Rating { get; set; }
        public RatingModel RatingModel { get; set; }

        //public List<Dish> Dishes { get; set; }
        public List<DishModel> Dishes { get; set; }
        public List<CommentModel> Comments { get; set; }
    }
}
