﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurants.Domain.Dtos
{
    public class CommentModel
    {
        public int RestuarantId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
