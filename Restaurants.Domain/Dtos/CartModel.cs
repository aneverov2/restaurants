﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantsCore.Models;

namespace Restaurants.Domain.Dtos
{
    public class CartModel : Model
    {
        public virtual ICollection<CartDto> Carts { get; set; }

        public int TotalPrice { get; set; }

        public CartModel()
        {
            Carts = new List<CartDto>();
        }
    }
}
